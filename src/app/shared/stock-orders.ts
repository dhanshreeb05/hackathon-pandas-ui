export class StockOrders {
    constructor(){
        this.id=0;
        this.stockTicker="";
        this.stockPrice=0.0;
        this.volume=0;
        this.buyOrSell="";
        this.statusCode=0;
        
    }
    id: number;
    stockTicker: string;
    stockPrice: number;
    volume : number;
    buyOrSell: string;
    statusCode: number;
   

}
