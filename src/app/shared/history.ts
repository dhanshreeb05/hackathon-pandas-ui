import { Timestamp } from "rxjs/internal/operators/timestamp";

export class History {
    constructor(){
        this.id=0;
        this.stockTicker="";
        this.stockPrice=0.0;
        this.volume=0;
        this.buyOrSell="";
        this.statusCode=0;
        this.dateTime="";
    }
    id: number;
    stockTicker: string;
    stockPrice: number;
    volume : number;
    buyOrSell: string;
    statusCode: number;
    dateTime: string;
}
