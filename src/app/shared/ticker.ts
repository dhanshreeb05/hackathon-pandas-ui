export class Ticker {
    constructor(){
        this.companyName = '';
        this.latestPrice = 0.0;
        this.iexBidPrice=0.0;
        this.iexAskPrice=0.0;
        this.change=0.0;
        this.iexVolume=0;
        this.week52Low=0.0;
        this.week52High=0.0;
        this.symbol ='';
        this.qt=0;
        this.est= this.qt*this.iexAskPrice;
    }
    companyName: String;
    latestPrice: number;
    iexBidPrice: number;
    iexAskPrice: number;
    change: number;
    iexVolume: number;
    week52Low: number;
    week52High: number;
    symbol: string;
    qt: number;
    est: number
}   