import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StockOrders } from '../shared/stock-orders';
import {History} from '../shared/history';
import { Ticker } from '../shared/ticker';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StockOrderApiService {
  apiURL = 'http://localhost:8080/api/stockOrder/';
  apiURL2 = 'http://localhost:8080/api/stockOrder/merge/';
  historyURL = 'http://localhost:8080/api/stockOrder/getHistory/';
  stockURL = 'https://cloud.iexapis.com/stable/stock/';
  apiURL1='http://localhost:8080/api/stockOrder/search/';
  top3URL = 'http://localhost:8080/api/stockOrder/getTop3/';

  url = '';

  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }  

  getStockDetails(sym:any):Observable<StockOrders>
{
  this.url=this.apiURL2+sym;
  return this.http.get<StockOrders>(this.url)
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}


  getTickerDetails(sym: string): Observable<Ticker> {
    this.url = this.stockURL+ sym + "/quote?token=pk_c69609262521425fa7a66e67053a0506";
    return this.http.get<Ticker>(this.url)
    .pipe(
      retry(1),
      catchError(this.handleError)
  )
  }


  
  // HttpClient API get() method 
  getAllOrders(): Observable<StockOrders> {
    return this.http.get<StockOrders>(this.apiURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
      
    )
  }

  getHistory(): Observable<History> {
    return this.http.get<History>(this.historyURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getTop3(): Observable<History> {
    return this.http.get<History>(this.top3URL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API get() method 
  getOrderById(id:any): Observable<StockOrders> {
    return this.http.get<StockOrders>(this.apiURL + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  // HttpClient API post() method 
  addOrder(stockOrder:StockOrders): Observable<StockOrders> {
    return this.http.post<StockOrders>(this.apiURL + '', JSON.stringify(stockOrder), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  } 

  // HttpClient API put() method
  editOrder(id:number, stockOrder:StockOrders): Observable<StockOrders> {
    return this.http.put<StockOrders>(this.apiURL, JSON.stringify(stockOrder), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API delete() method 
  deleteOrder(id:number){
    return this.http.delete<StockOrders>(this.apiURL + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // Error handling 
  handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }
 getMergeOrders(): Observable<StockOrders> {
  return this.http.get<StockOrders>(this.apiURL2)
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}

getSearchOrders(stockTicker:any): Observable<StockOrders> {
  return this.http.get<StockOrders>(this.apiURL1+stockTicker)
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}

  
}