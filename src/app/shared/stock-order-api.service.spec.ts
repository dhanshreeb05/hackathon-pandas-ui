import { TestBed } from '@angular/core/testing';

import { StockOrderApiService } from './stock-order-api.service';

describe('StockOrderApiService', () => {
  let service: StockOrderApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StockOrderApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
