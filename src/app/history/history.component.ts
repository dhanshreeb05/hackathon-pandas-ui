import { Component, OnInit } from '@angular/core';
import { observable, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { StockOrderApiService } from '../shared/stock-order-api.service';
import {StockOrders} from '../shared/stock-orders';


const stockOrders: StockOrders[] = [
  {
    id: 1,
    stockTicker: 'Stock1',
    stockPrice: 100,
    volume: 10,
    buyOrSell:'0',
    statusCode:0
  },
  {
    id: 2,
    stockTicker: 'Stock2',
    stockPrice: 12,
    volume: 10,
    buyOrSell:'0',
    statusCode:0
  },
  {
    id: 3,
    stockTicker: 'Stock3',
    stockPrice: 100,
    volume: 10,
    buyOrSell:'1',
    statusCode:0
  },
  {
    id: 4,
    stockTicker: 'Stock4',
    stockPrice: 100,
    volume: 10,
    buyOrSell:'1',
    statusCode:0
  },
  {
    id: 5,
    stockTicker: 'Stock5',
    stockPrice: 100,
    volume: 10,
    buyOrSell:'1',
    statusCode:0
  }
];

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  stockorder:any;
  constructor(public serviceobj:StockOrderApiService) {
   }

  ngOnInit(): void {
    this.serviceobj.getHistory().subscribe(data  => this.stockorder =data);
    console.log("orders", this.stockorder);

  }

  stockorders = stockOrders
}
