import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {StockOrderApiService} from './shared/stock-order-api.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomepageComponent } from './homepage/homepage.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { BuyStocksComponent } from './buy-stocks/buy-stocks.component';
import { HistoryComponent } from './history/history.component';
import { SellStocksComponent } from './sell-stocks/sell-stocks.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    PortfolioComponent,
    BuyStocksComponent,
    HistoryComponent,
    SellStocksComponent
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
