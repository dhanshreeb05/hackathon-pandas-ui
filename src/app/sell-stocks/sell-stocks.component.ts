import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StockOrderApiService } from "../shared/stock-order-api.service";
import { Ticker } from "../shared/ticker";
import { StockOrders } from "../shared/stock-orders";


@Component({
  selector: 'app-sell-stocks',
  templateUrl: './sell-stocks.component.html',
  styleUrls: ['./sell-stocks.component.css']
})
export class SellStocksComponent implements OnInit {



  StockOrders:any=[];
  @Input() stockTicker = {id:0, qt:0, est:0.0 }
  
  

  Ticker: any = [];
  @Input() tickerName = { symbol:'', qt:0, est:0.0 }
  @Input() order = {id:0, stockTicker:'', stockPrice:0.0, volume:0, buyOrSell:'', statusCode:0}
  constructor(
    public restApi: StockOrderApiService, 
    public router: Router,
    //public tick: Ticker
  ) { }

  ngOnInit() { }

stockDetails(sym:any)
{
  this.stockTicker.qt=0;
  this.restApi.getOrderById(sym).subscribe((data: {}) => {
    this.StockOrders = data;
    this.tickerDetails(this.StockOrders.stockTicker);
  })
}
  tickerDetails(sym: any) {
    this.tickerName.qt = 0;
    this.tickerName.est = 0.0;
    this.restApi.getTickerDetails(sym).subscribe((data: {}) => {
      this.Ticker = data;
    })
  }
  estimate(ap:any, qt:any) {
      this.tickerName.est = ap*qt;
  }
 
  sell(pr:any) {
    //if((this.StockOrders.id ||this.StockOrders.stockTicker || this.StockOrders.stockPrice || this.StockOrders.volume)==null)
    //{
      //window.alert("Enter valid details.Field(s) cannot be empty")
    
   // }
    if(this.StockOrders.volume==this.tickerName.qt)
    {
     // window.alert('Not enough quantity to sell')
      this.restApi.deleteOrder(this.StockOrders.id).subscribe((data:{}) => {
        this.router.navigate(['/portfolio'])
      })
    }
    if(this.tickerName.qt==0){
      window.alert('Enter a quantity to sell!!');
  }
  else if(this.tickerName.qt>this.StockOrders.volume){
    window.alert('Quantity must be less than or equal to volume!!');
}
    else if(this.StockOrders.volume!=this.tickerName.qt && this.StockOrders.volume>this.tickerName.qt && this.tickerName.qt!=0){
     if( window.confirm('Are you sure, you want to sell the stock?'))
     {
      this.order.id=this.StockOrders.id;
      this.order.stockTicker = this.StockOrders.stockTicker;
      this.order.stockPrice = this.StockOrders.stockPrice;
      this.order.buyOrSell = this.tickerName.qt.toString();
      this.order.statusCode = 0;
      this.order.volume=this.StockOrders.volume-this.tickerName.qt;
      //console.log(this.)
      this.restApi.editOrder(this.order.id,this.order).subscribe((data: {}) => {
        this.router.navigate(['/portfolio'])
      })

  }}
}
}