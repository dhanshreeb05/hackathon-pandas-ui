import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BuyStocksComponent } from './buy-stocks/buy-stocks.component';
import { HistoryComponent } from './history/history.component';
import { HomepageComponent } from './homepage/homepage.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { SellStocksComponent } from './sell-stocks/sell-stocks.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'homepage' },
  { path:'homepage', component: HomepageComponent},
  { path: 'buystocks', component: BuyStocksComponent },
  { path: 'sellstocks', component:  SellStocksComponent},
  { path: 'history', component: HistoryComponent },
  { path: 'portfolio', component: PortfolioComponent} 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
