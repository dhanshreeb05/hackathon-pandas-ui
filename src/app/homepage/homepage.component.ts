import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    this.carousel();
  }
  slideIndex = 0;
  
  
  carousel() {
    var i;
    var x = Array.from(document.getElementsByClassName("mySlides") as HTMLCollectionOf<HTMLElement>);
    for (i = 0; i < x.length; i++) {
      x[i].style.display = "none"; 
    }
    this.slideIndex++;
    if (this.slideIndex > x.length) {this.slideIndex = 1} 
    x[this.slideIndex-1].style.display = "block"; 
    setTimeout(() => this.carousel(), 2000);
  }

}
