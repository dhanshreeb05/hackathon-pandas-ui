import { Component, OnInit } from '@angular/core';
import { StockOrderApiService } from '../shared/stock-order-api.service';
import {StockOrders} from '../shared/stock-orders';



@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {


  public symbol :any;
  public Name="{{Name}}";
  public id ="{{id}}"
  public Volume="{{Volume}}";
  public top3:any;
 

  
  constructor(private _servicestock:StockOrderApiService) { }

  ngOnInit():void {
   this._servicestock.getMergeOrders()
    .subscribe(data  => {this.symbol =data;
    console.log(this.symbol)

   
  });
  this._servicestock.getTop3().subscribe(data => {this.top3 = data;});
    

}
onProjectselect():any
{
  this._servicestock.getMergeOrders()
    .subscribe(data  => {this.symbol =data;
    console.log(this.symbol)

    
  });
}
}