import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StockOrderApiService } from "../shared/stock-order-api.service";
import { Ticker } from "../shared/ticker";

@Component({
  selector: 'app-buy-stocks',
  templateUrl: './buy-stocks.component.html',
  styleUrls: ['./buy-stocks.component.css']
})
export class BuyStocksComponent implements OnInit {
s: any="";
  Ticker: any = [];
  @Input() tickerName = { symbol:'', qt:0, est:0.0 }
  @Input() order = {id:0, stockTicker:'', stockPrice:0.0, volume:0, buyOrSell:'', statusCode:0}
  constructor(
    public restApi: StockOrderApiService, 
    public router: Router,
    //public tick: Ticker
  ) { }

  ngOnInit() { }


  tickerDetails(sym: any) {
    this.tickerName.qt = 0;
    this.tickerName.est = 0.0;
    this.restApi.getTickerDetails(sym).subscribe((data: {}) => {
      this.Ticker = data;
     // console.log(this.Ticker.iexVolume);
      if(this.Ticker.iexVolume==0 || (!this.Ticker.iexVolume))
      {
        this.Ticker.iexVolume=100;
      }
    })
  }
  estimate(ap:any, qt:any) {
    if(this.tickerName.qt == 0){ window.alert('Enter an estimation quantity!!');}
    else{
      this.tickerName.est = ap*qt;}
  }
  buy(pr:any) {
    if(this.tickerName.est == 0){ window.alert('Estimate your cost before proceeding');}
    if(this.tickerName.est!=0){
      if (window.confirm('Are you sure, you want to place the order?')){
        this.s=this.tickerName.symbol;
        this.s=this.s.toUpperCase();
        this.order.stockTicker = this.s;
        this.order.stockPrice = pr;
        this.order.buyOrSell = '0';
        this.order.statusCode = 0;
        this.order.volume = this.tickerName.qt;
        console.log(this.restApi.getSearchOrders(this.order.stockTicker))
        this.restApi.addOrder(this.order).subscribe((data: {}) => {
          this.router.navigate(['/portfolio'])
      })

      }
    }
  }
} 
